#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <sstream>

#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <bitset>
#include <queue>
#include <deque>
#include <stack>

#include <math.h>
#include <cmath>
#include <string>
#include <cstring>
#include <string.h>

#include <memory.h>
#include <cassert>
#include <time.h>
#include <typeinfo>
#include <sys/time.h>

using namespace std;

#define forn(i,n) for (int i = 0; i < (int)(n); i++)
#define fornd(i, n) for (int i = (int)(n) - 1; i >= 0; i--)
#define forab(i,a,b) for (int i = (int)(a); i <= (int)(b); i++)
#define forabd(i, b, a) for (int i = (int)(b); i >= (int)(a); i--)
#define forit(i, a) for (__typeof((a).begin()) i = (a).begin(); i != (a).end(); i++)

#define _(a, val) memset (a, val, sizeof (a))
#define sz(a) (int)((a).size())
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()

typedef long long lint;
typedef unsigned long long ull;
typedef long double ld;
typedef pair<int, int> pii;
typedef vector<int> vii;

const lint LINF = 1000000000000000000LL;
const int INF = 1000000000;
const long double eps = 1e-9;
const long double PI = 3.1415926535897932384626433832795l;

#ifdef MY_DEBUG
#define dbgx( x ) { cerr << #x << " = " << x << endl; }
#define dbg( ... ) { fprintf(stderr, __VA_ARGS__); fflush(stderr); }
#else
#define dbgx( x ) {  }
#define dbg( ... ) {  }
#endif


double get_time() {
  timeval tv;
  gettimeofday(&tv, 0);
  return tv.tv_sec + tv.tv_usec * 1e-6;
}

#define WITH_TIME 1

const double TIME_LIMIT = 10.0;
const double GAP = 0.5;
double START_TIME;

bool there_is_a_time(double gap) {
#ifdef WITH_TIME
  return get_time() - START_TIME + gap < TIME_LIMIT;
#else
  return true;
#endif
}

int rand_int(int L, int R) {
  static uniform_int_distribution<int> d;
  static default_random_engine re;
  uniform_int_distribution<int>::param_type p{L, R};
  return d(re, p);
}

double rand_dbl() {
  static uniform_real_distribution<double> d;
  static default_random_engine re;
  return d(re);
}

void my_assert(bool stmt, const string &s) {
  if (!stmt) {
    dbgx(s);
    throw new exception();
  }
}

int gcd(int a, int b) {
  while (!a && !b) {
    int c = b;
    b = a % b;
    a = c;
  }
  return a + b;
}

struct vec {
  int x, y;
  vec() {}
  vec(int x, int y) { this->x = x; this->y = y; }
  vec operator+(const vec &oth) const { return vec(x + oth.x, y + oth.y); }
  vec operator-(const vec &oth) const { return vec(x - oth.x, y - oth.y); }
  vec operator/(int k) const { return vec(x/k, y/k); }
  vec operator*(int k) const { return vec(x*k, y*k); }
  int operator*(const vec &oth) const { return x*oth.x + y*oth.y; }
  int operator%(const vec &oth) const { return x*oth.y - y*oth.x; }
  bool operator<(const vec &oth) const { return x < oth.x || (x == oth.x && y < oth.y); }
  bool operator==(const vec &oth) const { return x == oth.x && y == oth.y; }
  bool operator!=(const vec &oth) const { return x != oth.x || y != oth.y; }
  void normalize() { int g = gcd(abs(x), abs(y)); x /= g; y /= g; }
};

int get_length(const vec &a, const vec &b) {
  return (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y);
}

struct Line {
  vec a, b;

  Line() : a(-1, -1), b(-1, -1) {}

  Line(const vec &first, const vec &second) {
    a = first;
    b = second;
    my_assert(a != b, "Line is given as a pair of the same points");
  }

  bool vec_on_line(const vec &v) const {
    return ((v - a) % (b - a)) == 0;
  }

  bool get_side(const vec &v) const {
    return ((v - a) % (b - a)) > 0;
  }

  bool on_different_sides(const vec &first, const vec &second) const {
    return get_side(first) != get_side(second);
  }
};

struct Interval {
  int l, r;
  Interval() : l(numeric_limits<int>::min()), r(numeric_limits<int>::max()) {}
  Interval(int l, int r) : l(l), r(r) {}
  Interval intersect(const Interval &oth) const {
    if (r < oth.l || oth.r < l)
      return Interval();
    return Interval(max(l, oth.l), min(r, oth.r));
  }
};

const int MAX_PLANTS = 105 + 15;
const int MAX_ROOTS = 105000 + 15;
const int MAX_LEN_ROOT = 100 + 15;
double pre_sqrt[MAX_LEN_ROOT*MAX_LEN_ROOT];
const int BOARD_SIZE = 1024;

int NP;
vector<int> g[MAX_ROOTS];
vec p[MAX_ROOTS];
Line lines[MAX_PLANTS][MAX_PLANTS][MAX_PLANTS];
int p_lines[MAX_PLANTS][MAX_PLANTS];

void calc_pre_sqrt() {
  for (int i = 0; i < MAX_LEN_ROOT*MAX_LEN_ROOT; ++i) {
    pre_sqrt[i] = sqrt(i);
  }
}

bool check_bounds(const vec &v) {
  return 0 <= v.x && v.x <= BOARD_SIZE && 0 <= v.y && v.y <= BOARD_SIZE;
}

struct Trie {
  int next[MAX_PLANTS*MAX_PLANTS*5][2];
  int free;
  int end[MAX_PLANTS*MAX_PLANTS*5];
  int queue_ends[MAX_PLANTS];
  int p_queue;

  Trie() {
    p_queue = 0;
    _(end, 0);
    clear();
  }

  void clear() {
    next[0][0] = next[0][1] = -1;
    free = 1;
    for (int i = 0; i < p_queue; ++i)
      end[queue_ends[i]] = 0;
    p_queue = 0;
  }

  bool add(int *state, int n) {
    int cur = 0;
    for (int i = 0; i < n; ++i) {
      if (next[cur][state[i]] == -1) {
        next[free][0] = next[free][1] = -1;
        next[cur][state[i]] = free++;
      }
      cur = next[cur][state[i]];
    }
    if (end[cur])
      return false;
    end[cur] = 1;
    queue_ends[p_queue++] = cur;
    return true;
  }
} trie;

bool check_all_plants_are_separated(const vector<Line> &result) {
  if (result.empty())
    return false;
  static int state[4*MAX_PLANTS];
  trie.clear();
  for (int i = 0; i < NP; ++i) {
    bool plant_on_line = false;
    for (int j = 0; j < sz(result) && !plant_on_line; ++j) {
      state[j] = result[j].get_side(p[i]);
      plant_on_line = plant_on_line || result[j].vec_on_line(p[i]);
    }
    if (!plant_on_line) {
      if (!trie.add(state, sz(result))) {
        return false;
      }
    }
  }
  return true;
}

bool check_two_plants_are_separated(const vector<Line> &result, const vec &first, const vec &second) {
  for (int i = 0; i < sz(result); ++i) {
    if (result[i].vec_on_line(first) || result[i].vec_on_line(second))
      return true;
    if (result[i].on_different_sides(first, second))
      return true;
  }
  return false;
}

bool check_plant_lies_on_some_line(const vec &plant, const vector<Line> &result) {
  for (int i = 0; i < sz(result); ++i)
    if (result[i].vec_on_line(plant))
      return true;
  return false;
}

double dfs(int v, int parent, const vector<Line> &result) {
  if (check_plant_lies_on_some_line(p[v], result))
    return 0.0;
  double score = 0.0;
  if (parent != -1) {
    if (check_two_plants_are_separated(result, p[v], p[parent])) {
// TODO: calc exact score
      return 0.0;
    }
    score += pre_sqrt[get_length(p[v], p[parent])];
  }

  for (int u : g[v]) {
    if (u != parent)
      score += dfs(u, v, result);
  }

  return score;
}

double calc_score_from_plant(int v, const vector<Line> &result) {
  return dfs(v, -1, result);
}

double get_score(const vector<Line> &result) {
  double score = 0.0;

  for (int num_plant = 0; num_plant < NP; ++num_plant) {
    score += calc_score_from_plant(num_plant, result);
  }

  return score;
}

Line get_line(int first_plant, int second_plant, int num_line) {
  my_assert(first_plant != second_plant, "get_line:: plants must be distinct");
  if (first_plant > second_plant)
    swap(first_plant, second_plant);
  my_assert(lines[first_plant][second_plant][num_line].a.x != -1, "get_line: kosyak");
  return lines[first_plant][second_plant][num_line];
}

void build_lines() {
  _(p_lines, 0);
  vec plants[MAX_PLANTS];
  for (int i = 0; i < NP; ++i)
    plants[i] = p[i];
  for (int i = 0; i < NP; ++i) {
    for (int j = i + 1; j < NP; ++j) {
      vec center = (p[i] + p[j]) / 2;
      auto get = [&](const vec &point) {
        if (point.y < center.y || (point.y == center.y && point.x <= center.x))
          return 0;
        return 1;
      };
      sort(plants, plants + NP, [&](const vec &a, const vec &b) {
        int position_a = get(a);
        int position_b = get(b);
        if (position_a != position_b)
          return position_a < position_b;
        return ((a - center) % (b - center)) > 0;
      });
      for (int k = 0; k < NP; k++) {
        vec second_center = (plants[k] + plants[(k + 1) % NP]) / 2;
        if (center != second_center) {
          lines[i][j][p_lines[i][j]] = Line(center, second_center);
          if (lines[i][j][p_lines[i][j]].on_different_sides(p[i], p[j])) {
            if (!lines[i][j][p_lines[i][j]].vec_on_line(p[i]))
              ++p_lines[i][j];
          }
        }
      }
      // lines[i][j][p_lines[i][j]++] = Line(p[i], p[j]);
    }
  }
}

vector<Line> random_choose_of_lines() {
  vector<Line> result;
  while (!check_all_plants_are_separated(result)) {
    int first_plant, second_plant, num_line;
    first_plant = rand_int(0, NP - 1);
    if (first_plant == NP - 1)
      --first_plant;
    second_plant = rand_int(first_plant + 1, NP - 1);
    if (!check_two_plants_are_separated(result, p[first_plant], p[second_plant])) {
      num_line = rand_int(0, p_lines[first_plant][second_plant] - 1);
      Line line = get_line(first_plant, second_plant, num_line);
      result.pb(line);
    }
  }
#ifdef MY_DEBUG
  for (int i = 0; i < NP; ++i) {
    for (int j = i + 1; j < NP; ++j) {
      my_assert(check_two_plants_are_separated(result, p[i], p[j]), "check_all_plants_are_separated is wrong");
    }
  }
#endif
  my_assert(sz(result) <= 4*NP, "random_choose_of_lines cannot find 4*NP lines that are able to separate plants");
  return result;
}

Line parallel_shift(const Line &line, double temperature) {
  my_assert(line.a != line.b, "parallel shift: input line is borken");
  vec shift;
  const int radius = 15;
  do {
    shift = vec(rand_int(-radius, radius), rand_int(-radius, radius));
  } while (!check_bounds(line.a + shift) || !check_bounds(line.b + shift));
  return Line(line.a + shift, line.b + shift);
}

Interval get_interval(int x, int d) {
  if (d == 0)
    return Interval();
  Interval result;
  if (d > 0) {
    result.l = -(x / d);
    result.r = (BOARD_SIZE - x) / d;
  } else {
    result.r = -x / d;
    result.l = (BOARD_SIZE - x) / d;
  }
  return result;
}

Interval find_interval(const vec &c, const vec &dir) {
#ifdef MY_DEBUG
  auto i = get_interval(c.x, dir.x).intersect(get_interval(c.y, dir.y));
  my_assert(check_bounds(c + dir*i.l), "find_interval is wrong");
#endif
  return get_interval(c.x, dir.x).intersect(get_interval(c.y, dir.y));
}

Line rotate_line(const Line &line, double temperature) {
  my_assert(line.a != line.b, "rotate_line: bad input line");
  vec direction_vec = line.b - line.a;
  direction_vec.normalize();
  Interval interval_k = find_interval(line.a, direction_vec);
  int k = rand_int(interval_k.l, interval_k.r);
  vec center = line.a + (direction_vec*k);
  my_assert(line.vec_on_line(center), "rotate_line: center doesn't lie on line");
  vec second_center;
  const int radius = 15;
  do {
    second_center = vec(center.x + rand_int(-radius, radius), center.y + rand_int(-radius, radius));
  } while (!check_bounds(second_center) || (second_center == center));
  return Line(center, second_center);
}

Line change_line(const Line &line, double temperature) {
  return line;
}

vector<Line> create_neighboring_solution(const vector<Line> &result, double temperature) {
  vector<Line> new_solution = result;

  enum class change_type{parallel_shift, rotate, change_line};
  change_type change = (change_type )rand_int(0, 2);
  int times = 1;
  for (int i = 0; i < times; ++i) {
    int id = rand_int(0, sz(new_solution) - 1);
    if ((change_type)change_type::parallel_shift == change) {
      new_solution[id] = parallel_shift(new_solution[id], temperature);
    }
    if ((change_type)change_type::rotate == change) {
      new_solution[id] = rotate_line(new_solution[id], temperature);
    }
    if ((change_type)change_type::change_line == change) {
      new_solution[id] = change_line(new_solution[id], temperature);
    }
  }
  return new_solution;
}

double calculate_temperature(int i, double temp_max) {
  return temp_max / i;
}

bool should_accept(double rnd, double score, double temperature) {
  double value = -score / temperature;
  if (value > 0)
    return true;
  if (value < -20)
    return false;
  return rnd < exp(value);
}

vector<Line> simulated_annealing(double temp_max) {
  vector<Line> best_result = random_choose_of_lines();
  double best_score = get_score(best_result);
  vector<Line> cur_result = best_result;
  double cur_score = best_score;
  int i = 1;
  while (there_is_a_time(0.6)) {
    double temp_cur = calculate_temperature(i, temp_max);
    vector<Line> state = create_neighboring_solution(cur_result, temp_cur);
    double state_score = get_score(state);
    double rnd = rand_dbl();
    if (should_accept(rnd, state_score - cur_score, temp_cur)) {
      cur_result = state;
      cur_score = state_score;
      if (state_score > best_score) {
        best_result = state;
        best_score = state_score;
      }
    }
    ++i;
  }
  dbgx(i);
  return best_result;
}

vector<Line> best_random_solution() {
  vector<Line> result = random_choose_of_lines();
  double score = get_score(result);
  while (there_is_a_time(1.0)) {
    auto cur_solution = random_choose_of_lines();
    double cur_score = get_score(cur_solution);
    if (score < cur_score) {
      score = cur_score;
      result = cur_solution;
    }
  }
  return result;
}

class CutTheRoots {
public:
  vector<int> makeCuts(int localNP, vector<int> points, vector<int> roots) {
    START_TIME = get_time();
    calc_pre_sqrt();
    NP = localNP;
    for (int i = 0; i < sz(points); i += 2) {
      p[i / 2] = vec(points[i], points[i + 1]);
    }
    for (int i = 0; i < sz(roots); i += 2) {
      int a = roots[i], b = roots[i + 1];
      g[a].pb(b);
      g[b].pb(a);
    }
    build_lines();
    vector<Line> result = simulated_annealing(100);
    vector<int> answer;
    for (int i = 0; i < sz(result); ++i) {
      answer.pb(result[i].a.x);
      answer.pb(result[i].a.y);
      answer.pb(result[i].b.x);
      answer.pb(result[i].b.y);
    }
    dbgx(get_time() - START_TIME);
    return answer;
  }
};

#ifdef MY_DEBUG
template<class T> void getVector(vector<T>& v) {
  for (size_t i = 0; i < v.size(); ++i) {
    scanf("%d", &v[i]);
  }
}

void test_build_lines() {
  NP = 5;
  p[0] = vec(2, 2);
  p[1] = vec(2, 4);
  p[2] = vec(4, 6);
  p[3] = vec(5, 4);
  p[4] = vec(2, 7);
  build_lines();
  for (int i = 0; i < NP; ++i) {
    for (int j = i + 1; j < NP; ++j) {
      dbg("((%d, %d), (%d, %d)):\n", p[i].x, p[i].y, p[j].x, p[j].y);
      for (int k = 0; k < p_lines[i][j]; ++k) {
        dbg("  (%d %d), (%d %d)\n", lines[i][j][k].a.x, lines[i][j][k].a.y, lines[i][j][k].b.x, lines[i][j][k].b.y);
      }
    }
  }
}

int main() {
  // freopen("/home/dima/TCO_MR1/testcase.txt", "r", stdin);
  int NP;
  scanf("%d", &NP);
  int Npoints = 0;
  scanf("%d", &Npoints);
  vector<int> points(Npoints);
  getVector(points);

  int Nroots = 0;
  scanf("%d", &Nroots);
  vector<int> roots(Nroots);
  getVector(roots);

  CutTheRoots cr;
  vector<int> ret = cr.makeCuts(NP, points, roots);

  printf("%d\n", (int)ret.size());
  for (int i = 0; i < (int)ret.size(); ++i) {
    printf("%d\n", ret[i]);
  }
  fflush(stdout);
  return 0;
}

#endif
