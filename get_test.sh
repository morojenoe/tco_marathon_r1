#!/usr/bin/env bash

program="get_testcase"
seed=1

g++ -O2 -DMY_DEBUG --std=c++0x -W -Wall -Wno-sign-compare -O2 -s -pipe -mmmx -msse -msse2 -msse3 ${program}.cpp -o ${program}.o

if [ "$?" == "0" ]
then
  java -jar tester.jar -exec "./${program}.o" -vis -save "output.png" -seed ${seed}
fi

rm get_testcase.o
