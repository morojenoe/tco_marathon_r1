#!/usr/bin/env bash
program="A"
cpp_file=${program}.cpp
exe_file=${program}.o
SEED=3

g++ -O2 -DMY_DEBUG --std=c++0x -W -Wall -Wno-sign-compare -O2 -s -pipe -mmmx -msse -msse2 -msse3 ${cpp_file} -o ${exe_file}

if [ "$?" == "0" ]
then 
  java -jar tester.jar -exec "./${exe_file}" -vis -save "output.png" -seed ${SEED}
fi
