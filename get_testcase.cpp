#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

using namespace std;

template<class T> void getVector(vector<T>& v) {
  for (int i = 0; i < v.size(); ++i)
    cin >> v[i];
}

template<class T> void printVector(vector<T>& v) {
  for (int i = 0; i < v.size(); ++i)
    cout << v[i] << endl;
}


int main() {
  printf("0\n");
  fflush(stdout);
  freopen("/home/dima/TCO_MR1/testcase.txt", "w", stdout);
  int NP;
  cin >> NP;
  cout << NP << endl;

  int Npoints;
  cin >> Npoints;
  cout << Npoints << endl;
  vector<int> points(Npoints);
  getVector(points);
  printVector(points);

  int Nroots;
  cin >> Nroots;
  cout << Nroots << endl;
  vector<int> roots(Nroots);
  getVector(roots);
  printVector(roots);

  return 0;
}
