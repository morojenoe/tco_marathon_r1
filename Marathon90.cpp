#define _CRT_SECURE_NO_DEPRECATE
#pragma comment(linker, "/STACK:128777216")

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <sstream>

#include <algorithm>
#include <vector>
#include <map>
#include <set>
#include <bitset>
#include <queue>
#include <deque>
#include <stack>

#include <math.h>
#include <cmath>
#include <string>
#include <cstring>
#include <string.h>

#include <memory.h>
#include <cassert>
#include <time.h>
#include <functional>
#include <random>

#ifndef MY_DEBUG
#include <sys/time.h>
#endif

using namespace std;

#define forn(i,n) for (int i = 0; i < (int)(n); i++)
#define fornd(i, n) for (int i = (int)(n) - 1; i >= 0; i--)
#define forab(i,a,b) for (int i = (int)(a); i <= (int)(b); i++)
#define forabd(i, b, a) for (int i = (int)(b); i >= (int)(a); i--)
#define forit(i, a) for (__typeof((a).begin()) i = (a).begin(); i != (a).end(); i++)

#define _(a, val) memset (a, val, sizeof (a))
#define sz(a) (int)((a).size())
#define pb push_back
#define mp make_pair
#define all(v) (v).begin(), (v).end()

typedef long long lint;
typedef unsigned long long ull;
typedef long double ld;
typedef pair<int, int> pii;
typedef vector<int> vii;
typedef unsigned int HASH;

const lint LINF = 1000000000000000000LL;
const int INF = 1000000000;

#ifdef MY_DEBUG
#define dbgx( x ) { cerr << #x << " = " << x << endl; }
#define dbg( ... ) { fprintf(stderr, __VA_ARGS__); fflush(stderr); }
#else
#define dbgx( x ) {  } 
#define dbg( ... ) {  } 
#endif

double getTime() 
{
#ifdef MY_DEBUG
	return (clock() / CLK_TCK);
#else
    timeval tv;
    gettimeofday(&tv, 0);
    return tv.tv_sec + tv.tv_usec * 1e-6;
#endif
}

#define WITH_TIME 1

double startTime;
const double TIME_LIMIT = 10.0;
const double GAP = 0.5;

bool thereIsATime(double gap)
{
#ifdef WITH_TIME
	return getTime() + gap < TIME_LIMIT - 0.1;
#else
	return true;
#endif
}

int random(int L, int R)
{
	static uniform_int_distribution<int> d;
	static default_random_engine re;
	uniform_int_distribution<int>::param_type p{L, R};
	return d(re, p);
}

void myassert(bool condition, string info)
{
	if (!condition)
		throw new exception(info.c_str());
}

struct Move
{
	short int r, c, dir;
	Move(){}
	Move(int r, int c, int dir) : r(r), c(c), dir(dir) {}
};

string move2str(const Move &move)
{
	char buf[15];
	sprintf(buf, "%d %d %d", move.r, move.c, move.dir);
	return (string)buf;
}

struct pos
{
	short int r, c, color;
	pos(){}
	pos(short int r, short int c, short int color) : r(r), c(c), color(color) {}
};

class HashTable
{
public:
	static const int AND = (1 << 22) - 1;
	static const int _Size = AND + 6;
	HASH hash[_Size];
	HASH prevBoardHash[_Size];
	Move move[_Size];
	int used[_Size];
	int pAllHashes, color;
	HASH allHashes[_Size];

	HashTable()
	{
		pAllHashes = 0;
		color = 1;
		_(used, 0);
		_(hash, 0);
		_(prevBoardHash, 0);
	}

	void clear()
	{
		color ++;
		pAllHashes = 0;
	}

	bool add(HASH curBoardHash, HASH _prevBoardHash, const Move &_move)
	{
		int p = curBoardHash & AND;
		while(used[p] == color && hash[p] != curBoardHash)
			p = (p + 1) & AND;
		if (used[p] != color)
		{
			used[p] = color;
			hash[p] = curBoardHash;
			prevBoardHash[p] = _prevBoardHash;
			move[p] = _move;

			allHashes[pAllHashes] = curBoardHash;
			return true;
		}

		return false;
	}

	int findPosition(HASH h)
	{
		int p = h & AND;
		while(used[p] == color && hash[p] != h)
			p = (p + 1) & AND;
		if (used[p] != color)
			return -1;
		return p;
	}
};

const char WALL = '#';
const char EMPTY = '.';

const int R_MAX = 60, C_MAX = 60;
const int COLOR_MAX = 10;
int BEAM_WIDTH;

const int dr[] = {0, 1, 0, -1};
const int dc[] = {-1, 0, 1, 0};

int R, C, ballsNumber;

char finalBoard[R_MAX + 5][C_MAX + 5];
char wallsOnBoard[R_MAX + 5][C_MAX + 5];
vector<pos> finalPositions;

HashTable hashTable;

HASH table[R_MAX*C_MAX][COLOR_MAX];

void init_zobrist()
{
	uniform_int_distribution<HASH> rng;
	mt19937 mt_engine;

	for(int i = 0; i < R_MAX*C_MAX; i++)
	{
		for(int j = 0; j < COLOR_MAX; j++)
		{
			table[i][j] = rng(mt_engine);
		}
	}
}

inline bool insideBoard(int r, int c)
{
	return 0 <= r && r < R && 0 <= c && c < C;
}

inline bool insideBoard(const pos &p)
{
	return 0 <= p.r && p.r < R && 0 <= p.c && p.c < C;
}

inline int getID(const pos &cur)
{
	return cur.r*C + cur.c;
}

struct Board
{
	vector<pos> b;
	HASH hash;
	Board()
	{
		b.resize(ballsNumber);
	}
};

HASH getHash(const Board &b)
{
	HASH h = 0;

	for(int i = 0; i < sz(b.b); i++)
	{
		int r, c, color;
		r = b.b[i].r;
		c = b.b[i].c;
		color = b.b[i].color;
		h ^= table[r*C + c][color];
	}

	return h;
}

HASH recalcHash(HASH prevHash, pos prev, pos next)
{
	prevHash ^= table[getID(prev)][prev.color];
	prevHash ^= table[getID(next)][next.color];
	return prevHash;
}

bool operator!=(const Board &a, const Board &b)
{
	return getHash(a) != getHash(b);
}

void drawBoard(const Board &curBoard, char brd[R_MAX + 5][C_MAX + 5])
{
	memcpy(brd, wallsOnBoard, sizeof(wallsOnBoard));

	for(int i = 0; i < sz(curBoard.b); i++)
	{
		pos p = curBoard.b[i];
		brd[p.r][p.c] = p.color + '0';
	}
}

//int getGameScore(const Board &curBoard)
//{
//	int score = 0;
//	for(int i = 0; i < sz(curBoard.b); i++)
//	{
//		pos p = curBoard.b[i];
//		if (finalBoard[p.r][p.c] != WALL && finalBoard[p.r][p.c] != EMPTY)
//		{
//			score += finalBoard[p.r][p.c] - '0' == p.color? 2 : 1;
//		}
//	}
//	return score;
//}

int getApproximateBallsNumber2FillPos(const Board &curBoard, pos targetPosition)
{
	static char brd[R_MAX + 5][C_MAX + 5];
	drawBoard(curBoard, brd);

	if (brd[targetPosition.r][targetPosition.c] != WALL && brd[targetPosition.r][targetPosition.c] != EMPTY)
	{
		if (brd[targetPosition.r][targetPosition.c] - '0' == targetPosition.color)
			return -2;
		return -1;
	}

	int ballsNumber = R_MAX;
	for(int i = 0; i < 4; i++)
	{
		pos p = targetPosition;
		int c = 1;
		p.r += dr[i];
		p.c += dc[i];
		while(insideBoard(p) && brd[p.r][p.c] == EMPTY)
			c ++, p.r += dr[i], p.c += dc[i];
		ballsNumber = min(ballsNumber, c);
	}
	return ballsNumber;
}

int getBoardScore(const Board &curBoard, pos targetPosition)
{
	return -getApproximateBallsNumber2FillPos(curBoard, targetPosition);
}

Board boards[2][10*R_MAX*C_MAX];
int pBoards[2];
pair<int, int> tmp_score_id[10*R_MAX*C_MAX];

void sort_boards(int k, pos targetPosition)
{
	for(int i = 0; i < pBoards[1]; i++)
		tmp_score_id[i] = mp(getBoardScore(boards[1][i], targetPosition), i);
	make_heap(tmp_score_id, tmp_score_id + pBoards[1]);
	for(int i = 0; i < k; i++)
	{
		int id = tmp_score_id[i].second;
		boards[0][i] = boards[1][id];
		pop_heap(tmp_score_id, tmp_score_id + pBoards[1]);
		pBoards[1] --;
	}
}

void generateNewStates(const Board &curBoard, Board *space, int &shift)
{
	static char brd[R_MAX + 5][C_MAX + 5];
	drawBoard(curBoard, brd);

	for(int i = 0; i < sz(curBoard.b); i++)
	{
		pos p = curBoard.b[i];
		for(int dir = 0; dir < 4; dir++)
		{
			int r = p.r, c = p.c;
			r += dr[dir];
			c += dc[dir];
			while(insideBoard(r, c) && brd[r][c] == EMPTY)
				r += dr[dir], c += dc[dir];
			r -= dr[dir];
			c -= dc[dir];

			if (r != p.r || c != p.c)
			{
				space[shift] = curBoard;
				space[shift].b[i] = pos(r, c, p.color);
				space[shift].hash = recalcHash(space[shift].hash, p, space[shift].b[i]);
				if (hashTable.add(space[shift].hash, curBoard.hash, Move(p.r, p.c, dir)))
					shift ++;
			}
		}
	}
}

void beamSearch(Board &startBoard, pos targetPosition)
{
	BEAM_WIDTH = 120;

	startBoard.hash = getHash(startBoard);
	hashTable.add(startBoard.hash, 0, Move(-1, -1, -1));

	boards[0][0] = startBoard;
	pBoards[0] = 1;
	pBoards[1] = 0;
	for(int curStep = 0; curStep < 50; curStep++)
	{
		for(int i = 0; i < pBoards[0]; i++)
		{
			int cachedScore = getBoardScore(boards[0][i], targetPosition);
			if (cachedScore > 0 /*cachedScore == 2 || curStep >= 30 && cachedScore == 1*/)
			{
				startBoard = boards[0][i];
				return;
			}
			generateNewStates(boards[0][i], boards[1], pBoards[1]);
		}

		pBoards[0] = min(BEAM_WIDTH, pBoards[1]);
		sort_boards(pBoards[0], targetPosition);
		pBoards[1] = 0;
	}
}

vector<string> recoverMoves(HASH bestHash)
{
	vector<string> result;

	int p = hashTable.findPosition(bestHash);
	while(hashTable.move[p].r != -1)
	{
		result.pb( move2str(hashTable.move[p]) );
		p = hashTable.findPosition(hashTable.prevBoardHash[p]);
	}

	reverse(all(result));
	return result;
}

vector<string> fillPosition(Board &board, pos targetPosition)
{
	hashTable.clear();

	static char copyWallsOnBoard[R_MAX + 5][C_MAX + 5];
	memcpy(copyWallsOnBoard, wallsOnBoard, sizeof(wallsOnBoard));

	static const int NUMBER_OF_BALLS_TO_FILL = 2;
	set<pair<int, int>> bestPositions;
	vector<pos> positions;
	for(int i = 0; i < sz(board.b); i++)
	{
		pos p = board.b[i];
		if (p.r == targetPosition.r && p.c == targetPosition.c)
			return vector<string>();

		if (finalBoard[p.r][p.c] - '0' == p.color)
			continue;

		bestPositions.insert( {abs(p.r - targetPosition.r) + abs(p.c - targetPosition.c), i} );
		if (bestPositions.size() > NUMBER_OF_BALLS_TO_FILL)
			bestPositions.erase(prev(bestPositions.end()));
	}

	for(auto elem : bestPositions)
		positions.pb( board.b[elem.second] );
	Board curBoard;
	curBoard.b = positions;

	for(int i = 0; i < sz(board.b); i++)
	{
		pos p = board.b[i];

		auto t = mp(abs(p.r - targetPosition.r) + abs(p.c - targetPosition.c), i);
		if (bestPositions.find(t) == bestPositions.end())
			wallsOnBoard[p.r][p.c] = WALL;
	}

	vector<string> result;
	beamSearch(curBoard, targetPosition);
	if (curBoard != board)
	{
		result = recoverMoves(curBoard.hash);
		for(int i = 0, j = 0; i < sz(board.b); i++)
		{
			pos p = board.b[i];

			auto t = mp(abs(p.r - targetPosition.r) + abs(p.c - targetPosition.c), i);
			if (bestPositions.find(t) != bestPositions.end())
			{
				board.b[i] = curBoard.b[j++];
			}
		}
	}

	memcpy(wallsOnBoard, copyWallsOnBoard, sizeof(wallsOnBoard));

	return result;
}

vector<string> solution(Board &board)
{
	sort(all(finalPositions), [&](const pos &a, const pos &b) {
		return getApproximateBallsNumber2FillPos(board, a) > getApproximateBallsNumber2FillPos(board, b);
	});

	shuffle(all(finalPositions), default_random_engine(0xAC));

	vector<string> result;

	while(sz(finalPositions) > 0 && thereIsATime(2*GAP))
	{
		for(int i = 0; i < sz(finalPositions); i++)
		{
			if (!thereIsATime(GAP))
				break;
			auto tmp = fillPosition(board, finalPositions[i]);
			if (sz(tmp) > 0)
				result.insert(result.end(), all(tmp));
		}
	}

	if (sz(result) > 20*ballsNumber)
		result.resize(20*ballsNumber);
	return result;
}

void printStatistic(Board board);

class RollingBalls
{
public:
	vector<string> restorePattern(vector<string> start, vector<string> target)
	{
		startTime = getTime();
		init_zobrist();
		R = sz(start);
		C = sz(start[0]);
		ballsNumber = 0;
		for(int i = 0; i < R; i++)
		{
			for(int j = 0; j < C; j++)
			{
				if (start[i][j] != WALL && start[i][j] != EMPTY)
					ballsNumber ++;
			}
		}

		Board initial;
		int cur = 0;
		for(int i = 0; i < R; i++)
		{
			for(int j = 0; j < C; j++)
			{
				if (start[i][j] != WALL && start[i][j] != EMPTY)
				{
					initial.b[cur++] = pos(i, j, start[i][j] - '0');
				}
			}
		}

		for(int i = 0; i < R; i++)
		{
			for(int j = 0; j < C; j++)
			{
				finalBoard[i][j] = target[i][j];
			}
		}

		for(int i = 0; i < R; i++)
		{
			for(int j = 0; j < C; j++)
			{
				wallsOnBoard[i][j] = EMPTY;
				if (start[i][j] == WALL)
					wallsOnBoard[i][j] = WALL;
			}
		}

		finalPositions.reserve( ballsNumber );
		for(int i = 0; i < R; i++)
		{
			for(int j = 0; j < C; j++)
			{
				if (target[i][j] != WALL && target[i][j] != EMPTY)
					finalPositions.pb( pos(i, j, target[i][j] - '0') );
			}
		}

		auto result = solution(initial);
#ifdef MY_DEBUG
		printStatistic(initial);
#endif
		return result;
	}
};

void printStatistic(Board brd)
{
	int onTheirPlaces = 0, onOtherPlaces = 0, colors = 0;
	for(int i = 0; i < sz(brd.b); i++)
	{
		pos p = brd.b[i];
		if (finalBoard[p.r][p.c] - '0' == p.color)
			onTheirPlaces ++;
		else if (finalBoard[p.r][p.c] != WALL && finalBoard[p.r][p.c] != EMPTY)
			onOtherPlaces ++;

		if (finalBoard[p.r][p.c] != WALL && finalBoard[p.r][p.c] != EMPTY)
			colors = max(colors, finalBoard[p.r][p.c] - '0');
	}

	dbg("Number of different colors = %d\n", colors);
	dbg("Number of balls = %d\n", ballsNumber);
	dbg("On right places = %d\n", onTheirPlaces);
	dbg("On other places = %d\n", onOtherPlaces);
	dbg("Not found places for %d\n", ballsNumber - onTheirPlaces - onOtherPlaces);
}

#ifdef MY_DEBUG
int main()
{
	//freopen("input.txt", "r", stdin);
	vector<string> start, target;
	int H;
	scanf("%d\n", &H);
	for(int i = 0; i < H; i++)
	{
		char tmp[155];
		scanf("%s", tmp);
		start.pb( tmp );
	}
	scanf("%d\n", &H);
	for(int i = 0; i < H; i++)
	{
		char tmp[155];
		scanf("%s", tmp);
		target.pb( tmp );
	}

	auto ret = RollingBalls().restorePattern(start, target);
	printf("%d\n", sz(ret));
	for(int i = 0; i < sz(ret); i++)
        printf("%s\n", ret[i].c_str());
	fflush(stdout);

	dbgx( getTime() - startTime );

	return 0;
}
#endif
